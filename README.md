# ics-ans-role-nginx-proxy

Ansible role to install nginx and use it as a proxy.

## Requirements

- ansible >= 2.7
- molecule >= 2.19

## Role Variables

2020-03-23: add the management of multiple websites:
The variable "nginx_proxy_multisites" enblae/disable the management of multiple website.
Each website will have it's own configuration file in /etc/nginx/conf.d
General Proxy settings are defined in /etc/nginx/proxy_setting.conf

websites are defined as follow:
```yaml
nginx_proxy_sites:
  - name: site1
    ip: "{{ ansible_default_ipv4.address }}"
    port: 80
    ssl_port: 443
    vhost: site1.ess.eu
    certfile: /etc/pki/site1.cert
    keyfile: /etc/pki/site1.key
    backends:
      - name: apache
        ip: 10.0.100.1
        port: 8080
        ssl_port: 8443
        location: "/"
        protocol: https
      - name: tomcat
        ip: 10.0.100.1
        port: 7080
        ssl_port: 7443
        location: "/tomcat"
        protocol: https
  - name: site2
    ip:  "{{ ansible_default_ipv4.address }}"
    port: 80
    ssl_port: 443
    vhost: site2.ess.eu
    certfile: /etc/pki/site2.cert
    keyfile: /etc/pki/site2.key
    backends:
      - name: apache
        ip: 10.0.100.2
        port: 8080
        ssl_port: 8443
        location: "/"
        protocol: https
      - name: tomcat
        ip: 10.0.100.2
        port: 7080
        ssl_port: 7443
        location: "/tomcat"
        protocol: https

```

```yaml
nginx_proxy_conf_dir: /etc/nginx
nginx_proxy_log_dir: /var/log/nginx

nginx_proxy_pass: http://127.0.0.1:8080

nginx_proxy_https_port: 443
nginx_proxy_http_port: 80

nginx_proxy_workers: 1
nginx_proxy_workers_conn: 1024

nginx_proxy_headers:
  - "Host $http_host"
  - "X-Forwarded-Host $host"
  - "X-Forwarded-Server $host"

nginx_proxy_local_users:
  - name: user1
    password: "apassword"  # Vault me!
  - name: user2
    password: "anotherpassword"  # Vault me!
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-nginx-proxy
```

## License

BSD 2-clause
